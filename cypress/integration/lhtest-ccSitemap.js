// we can access the Cypress.env() object outside the test
const sitemapUrls = Cypress.env('sitemapUrls')

describe('Google Lighthouse Node: Audit Web Performance Test of each url in sitemap', () => {
  before(() => {
    // confirm the users data has been set
    expect(sitemapUrls).to.be.an('array').and.not.be.empty
  })

  sitemapUrls.forEach((sitemapUrls) => {
    it(`Sitemap URL - ${sitemapUrls}`, () => {
        const thresholds = {
          // https://web.dev/performance-scoring/#metric-scores
            performance: 90,
            'first-contentful-paint': 1800,
            'interactive': 3800,
            'speed-index': 3400,
            'total-blocking-time': 200,
            'largest-contentful-paint': 2500,
            accessibility: 90,
            'best-practices':90,
            seo: 90,
            pwa: 90,
          };

        cy.visit(sitemapUrls)
        cy.lighthouse(thresholds);
    })
  })
})