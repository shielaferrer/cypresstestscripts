const got = require('got');
const { parseString } = require('xml2js');
const { lighthouse, pa11y, prepareAudit } = require("cypress-audit");




module.exports = async (on: any, config: any) => {
    on("before:browser:launch", (browser = {}, launchOptions) => {
        prepareAudit(launchOptions);
      });

    on('task', {
            lighthouse: lighthouse(lighthouseReport => {
            console.log(lighthouseReport)
        }),
      })

    await got('https://compareclub.com.au/sitemap.xml')
    .then((response: { body: any }) => {
      console.log('We got something');

      console.log(response.body);
      const sitemapUrls = [];

      parseString(response.body, function (err, result) {
        for (let url of result.urlset.url) {
          sitemapUrls.push(url.loc[0]);
        }
      });

      config.env.sitemapUrls = sitemapUrls;
    })
    .catch((error: any) => {
      console.log('We got nothing', error);
    });

  console.log(config.env.sitemapUrls);
  return config;
};
